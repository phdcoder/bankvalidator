Validador de contas bancárias em PhP - Ciência da Computação - IFG - Anápolis 
=============================================================================
Este conjunto de classes permite a validação de contas bancárias dos seguintes Bancos: Itaú, Bradesco, Banco do Brasil, Santander, HSBC.
Foi desenvolvido inicialmente pelos alunos do 2º período do bacharelado em Ciência da Computação do Instituto Federal de Goiás - Câmpus Anápolis como trabalho da disciplina de Programação Orientada a Objetos, sob a orientação do professor Alessandro Rodrigues e Silva. 

Requisitos:
===========
- PhP 7.0 ou superior


Exemplos de Uso: 
================


  
